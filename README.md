# NumberWizard

2D game based on Udemy Unity 2D tutorial from GameDev.tv. This uses a simple binary search to "guess" your number.

This was built using Unity 2018.2.2